const DELIMITER = "|||";
let selectedTurtle = undefined;
let inventory = {}
let selectedInventorySlot = 1;

function docReady(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function buildMessage(messageType, content) {
    return message = messageType + DELIMITER + content.join(DELIMITER)
}

function addAllTurtlesToDropdown(turtles) {
    console.log("turtles recived: ", turtles);
    const turtlesDropdown = document.getElementById("selectableTurtles");

    turtlesDropdown.innerHTML = "";
    //Create and append the options
    for (var i = 0; i < turtles.length; i++) {
        var option = document.createElement("option");
        option.value = turtles[i];
        option.text = turtles[i];
        turtlesDropdown.appendChild(option);
    }
}

function generateInventory() {
    const inventoryWrapper = document.getElementById("inventory-wrapper");
    inventoryWrapper.innerHTML = "";
    for (let index = 1; index < 17; index++) {
        var inventorySlot = document.createElement("div");
        const value = inventory[index];
        const selectedClass = index == selectedInventorySlot ? "selected" : "";
        console.log("selected: ", selectedClass)
        if (value) {
            inventorySlot.innerHTML = "<span class='" + selectedClass + "' title='" + value.name + ":" + value.damage + "'>" + value.count + "</span>";
        } else {
            inventorySlot.innerHTML = "<span class='" + selectedClass + "' title='empty'>0</span>";
        }

        inventoryWrapper.appendChild(inventorySlot);
    }
}

function requestInventoryUpdate(socket) {
    inventory = {};
    for (let index = 1; index < 17; index++) {
        socket.send(buildMessage("command", [selectedTurtle, "ws.send(\"inventory\" .. delimiter .. " + index + " .. delimiter .. textutils.serialiseJSON(turtle.getItemDetail(" + index + ")))"]));
    }
    requestSelectedInventorySlot(socket);
}

function requestSelectedInventorySlot(socket) {
    socket.send(buildMessage("command", [selectedTurtle, "ws.send(\"inventory\" .. delimiter .. \"selected\" .. delimiter .. turtle.getSelectedSlot())"]));
}

docReady(function() {
    let socket = new WebSocket("ws://localhost:8080");

    socket.onopen = function(e) {
        console.log("[open] Connection established");
        console.log("Sending to server");
        socket.send("interface" + DELIMITER + "web");
        socket.send("list");
    };

    socket.onmessage = function(event) {
        console.log("[message] Data received from server: ", event.data);
        const splitMessage = event.data.split(DELIMITER)
        if (splitMessage[0] === "list") {
            const listOfTurtles = JSON.parse(splitMessage[1]);
            addAllTurtlesToDropdown(listOfTurtles)
            if (!selectedTurtle) {
                selectedTurtle = listOfTurtles[0];
            }
            requestInventoryUpdate(socket);
        } else if (splitMessage[0] === "inventory") {
            if (splitMessage[1] === "selected") {
                selectedInventorySlot = splitMessage[2];
            } else {
                inventory[splitMessage[1]] = JSON.parse(splitMessage[2])
            }
            console.log("inventory: ", inventory);
            generateInventory();
        }
    };

    socket.onclose = function(event) {
        if (event.wasClean) {
            console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        } else {
            // e.g. server process killed or network down
            // event.code is usually 1006 in this case
            console.log('[close] Connection died');
        }
    };

    socket.onerror = function(error) {
        console.log(`[error] ${error.message}`);
    };

    document.getElementById("terminateTurtle").addEventListener("click", function() {
        socket.send(buildMessage("command", [selectedTurtle, "terminate"]));
    });
    document.getElementById("selectableTurtles").addEventListener("change", function(event) {
        console.log("select updated: ", event.target.options[event.target.selectedIndex].text);
        selectedTurtle = event.target.options[event.target.selectedIndex].text;
        requestInventoryUpdate(socket);
    });


    document.addEventListener('keyup', (e) => {
        console.log("CLICKED: ", e.code);
        if (e.code === "KeyW") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.forward()"]));
        } else if (e.code === "KeyS") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.back()"]));
        } else if (e.code === "KeyA") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.turnLeft()"]));
        } else if (e.code === "KeyD") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.turnRight()"]));
        } else if (e.code === "Space") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.up()"]));
        } else if (e.code === "ShiftLeft") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.down()"]));
        } else if (e.code === "KeyQ") {
            socket.send(buildMessage("command", [selectedTurtle, "turtle.dig()"]));
        } else if (e.code === "KeyU") {
            requestInventoryUpdate(socket);
        } else if (e.code === "KeyL") {
            socket.send("list");
        }
    });

});