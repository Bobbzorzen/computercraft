const WebSocket = require('ws');

const MESSAGE_TYPES = {
    "INTERFACE": "interface",
    "TURTLE": "turtle",
    "LIST": "list",
    "PRINT": "print"
}
const DELIMITER = "|||";

const turtles = {};
let interface = {};

const wss = new WebSocket.Server({
    port: 8080
});

function sendModelForPrintingToTurtle(ws, splitMessage) {
    const requestedTurtle = turtles[splitMessage[1]];
    if (requestedTurtle) {
        console.log("Sending ", splitMessage[2], " to turtle: ", splitMessage[1])
        requestedTurtle.send(splitMessage[2])
    } else {
        console.log("requested turtle isn't connected");
        ws.send("Turtle not found");
    }
}

console.log("starting the server");
wss.on('connection', function connection(ws) {
    console.log("Connection recived");
    ws.on('message', function incoming(message) {
        const splitMessage = message.split(DELIMITER);
        console.log('received: %s', splitMessage);
        if (splitMessage[0] == MESSAGE_TYPES.INTERFACE) {
            interface[splitMessage[1]] = ws;
        } else if (splitMessage[0] == MESSAGE_TYPES.TURTLE) {
            ws.turtleName = splitMessage[1];
            turtles[splitMessage[1]] = ws;
            if (interface) {
                interface["web"].send(MESSAGE_TYPES.LIST + DELIMITER + JSON.stringify(Object.keys(turtles)));
            }
        } else if (splitMessage[0] == MESSAGE_TYPES.LIST) {
            ws.send(MESSAGE_TYPES.LIST + DELIMITER + JSON.stringify(Object.keys(turtles)));
        } else if (splitMessage[0] == MESSAGE_TYPES.PRINT) {
            sendModelForPrintingToTurtle(ws, splitMessage);
        }
    });
    ws.on("close", () => {
        delete turtles[ws.turtleName];
        if (interface) {
            interface["web"].send(MESSAGE_TYPES.LIST + DELIMITER + JSON.stringify(Object.keys(turtles)));
        }
        console.log("connection lost");
    })
});