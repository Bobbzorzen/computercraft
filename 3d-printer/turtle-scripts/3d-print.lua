os.loadAPI("external-libs/json.lua")
environment = require "environment"
printingUtilities = require "printing-utilities"

ws, err = http.websocket(environment.connectionString)
delimiter = "|||"

if ws then
    -- Probably should do something interesting with the websocket before
    print("Socket established, sending identification")
    ws.send("turtle" .. delimiter .. os.getComputerLabel())
    while true do
        local _, url, response, isBinary = os.pullEvent("websocket_message")
        if (response == "terminate") then
            print("response: " .. response)
            break
        else
            print("response: " .. response)
            jsonObject = json.decode(response)
            printingUtilities.printModel(jsonObject)
        end
    end
    print("Closing socket")
    ws.close()
else
    print("no socket available")
    print(err)
end
