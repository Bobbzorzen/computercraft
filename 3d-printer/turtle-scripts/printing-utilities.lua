local printingUtilities = {}

function printingUtilities.selectNonEmptySlotIfNeeded()
    local emptyInventorySlots = 0
    while (turtle.getItemCount(turtle.getSelectedSlot()) == 0 and emptyInventorySlots < 17) do
        emptyInventorySlots = emptyInventorySlots + 1
        turtle.select((turtle.getSelectedSlot()) % 16 + 1)
    end
    if (emptyInventorySlots >= 17) then
        print("No more blocks!")
    end
end

function printingUtilities.checkRestOfLineForBlocks(line, startingPosition)
    startingPosition = startingPosition or 1
    local blocksLeft = 0
    for i = startingPosition, #line do
        if line[i] == 1 then
        blocksLeft = blocksLeft + 1
        end
    end
    return blocksLeft
end

function printingUtilities.printLine(line)
    local movesMade = 0
    for i in pairs(line) do
        if printingUtilities.checkRestOfLineForBlocks(line, i) == 0 then
            break
        end
        if line[i] == 1 then
            printingUtilities.selectNonEmptySlotIfNeeded()
            turtle.placeDown()
        end
        turtle.forward()
        movesMade = movesMade + 1
    end
    for i = 1, movesMade do
        turtle.back()
    end
end

function printingUtilities.printLayer(layer)
    turtle.up()
    for i in pairs(layer) do
        printingUtilities.printLine(layer[i])
        turtle.turnRight()
        turtle.forward()
        turtle.turnLeft()
    end
    turtle.turnLeft()
    for _ in pairs(layer) do
        turtle.forward()
    end
    turtle.turnRight()
end

function printingUtilities.printModel(model)
    turtle.forward()
    for i in pairs(model) do
        printingUtilities.printLayer(model[i])
    end
    turtle.back()
    for _ in pairs(model) do
        turtle.down()
    end
end

return printingUtilities
