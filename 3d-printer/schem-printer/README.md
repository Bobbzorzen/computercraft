# .SCHEM printing module

### TODO: 

* Add support for converting .SCHEM files to json (Look at nbt parsing https://github.com/MikuAuahDark/lua-nbt for lua and https://github.com/sjmulder/nbt-js for JS)
* Add support for loading .SCHEM files into the printer, both web interface and directly to printing turtle
