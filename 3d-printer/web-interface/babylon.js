let engine = "";
let scene = "";
let canvas = ""
let camer = "";
const BOX_SIZE = 20;

const centerOfSceneX = ((modelWidth * BOX_SIZE) / 2) - BOX_SIZE / 2
const centerOfSceneY = (model.length * BOX_SIZE) / 2 - BOX_SIZE / 2
const centerOfSceneZ = (modelDepth * BOX_SIZE) / 2 - BOX_SIZE / 2
const centerOfSceneVector = new BABYLON.Vector3(centerOfSceneX, centerOfSceneY, centerOfSceneZ);

function setupCamera() {
    const alpha = BABYLON.Tools.ToRadians(-45); // rotation around y axis
    const beta = BABYLON.Tools.ToRadians(45); // rotation around x axis
    const radius = 500;

    camera = new BABYLON.ArcRotateCamera("camera", alpha, beta, radius, centerOfSceneVector, scene);
    camera.attachControl(canvas, true);
}

function setupLight() {
    const light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0), scene);
    light.specular = new BABYLON.Color3(0, 0, 0);
    light.intensity = 0.7;
    scene.clearColor = new BABYLON.Color4(1, 1, 1);
}

function setupGround() {
    var ground = BABYLON.MeshBuilder.CreateGround("ground", {
        width: modelWidth * BOX_SIZE,
        height: modelDepth * BOX_SIZE
    }, scene);
    ground.position = centerOfSceneVector;
}

function renderLoop() {
    scene.render();
}

function addBlock(position) {
    const material = new BABYLON.StandardMaterial("material", scene);
    const color = BABYLON.Color3.FromHexString("#FF0000");
    const localSettings = {
        width: BOX_SIZE,
        height: BOX_SIZE,
        depth: BOX_SIZE
    }
    const box = BABYLON.MeshBuilder.CreateBox("block", localSettings, scene);
    material.diffuseColor = color
    box.material = material;
    box.position.x = position.x;
    box.position.y = position.y;
    box.position.z = position.z;
}

function removeAllMeshes() {
    while(mesh = scene.meshes.find(element => element.name !== "ground")) {
        scene.removeMesh(mesh);
    }
}

function printModel() {
    removeAllMeshes()
    for (let layerIndex = 0; layerIndex < model.length; layerIndex++) {
        const currentLayer = model[layerIndex];
        for (let rowIndex = 0; rowIndex < currentLayer.length; rowIndex++) {
            const currentRow = currentLayer[rowIndex];
            for (let blockIndex = 0; blockIndex < currentRow.length; blockIndex++) {
                const currentBlock = currentRow[blockIndex];
                if (currentBlock) {
                    const position = {
                        x: blockIndex * BOX_SIZE,
                        y: layerIndex * BOX_SIZE,
                        z: rowIndex * BOX_SIZE
                    }
                    addBlock(position);
                }
            }
        }
    }
}

docReady(function() {
    canvas = document.getElementById("babylon-canvas");
    engine = new BABYLON.Engine(canvas, true, {
        preserveDrawingBuffer: true,
        stencil: true
    });
    scene = new BABYLON.Scene(engine);
    setupLight();
    setupCamera();
    setupGround();
    engine.runRenderLoop(() => {
        renderLoop();
    });
});