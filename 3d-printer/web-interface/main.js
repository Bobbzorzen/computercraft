let selectedLayer = 0;
let model = [];
let modelWidth = 7;
let modelDepth = 7;

function docReady(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function toggleBlock(event) {
    const currentTarget = event.target;
    const currentBlock = currentTarget.parentElement;
    const currentRow = currentBlock.parentElement;
    const currentRowNumber = currentRow.dataset.rowid;
    const currentBlockNumber = currentBlock.dataset.blockid;
    if (currentTarget.dataset.value == 0) {
        currentTarget.classList.add("selected");
        currentTarget.title = "block";
        currentTarget.dataset.value = 1
        model[selectedLayer][currentRowNumber][currentBlockNumber] = 1;
    } else if (currentTarget.dataset.value == 1) {
        currentTarget.classList.remove("selected");
        currentTarget.title = "no block";
        currentTarget.dataset.value = 0
        model[selectedLayer][currentRowNumber][currentBlockNumber] = 0;
    }
    printModel();
}

function addAllLayersToDropdown() {
    const layersDrowpdown = document.getElementById("layers");

    layersDrowpdown.innerHTML = "";
    //Create and append the options
    for (var i = 0; i < model.length; i++) {
        var option = document.createElement("option");
        option.value = i;
        option.text = "layer: " + i;
        option.selected = i == selectedLayer ? "selected": "";
        layersDrowpdown.appendChild(option);
    }
}

function displayLayer() {
    const boxSize = 50;
    const layerWrapper = document.getElementById("layer-wrapper");
    layerWrapper.style.width = "" + (boxSize * modelWidth) + "px"
    layerWrapper.style.height = "" + (boxSize * modelDepth) + "px"
    layerWrapper.innerHTML = "<img class='turtle' src='images/turtle-icon.png'>";
    for (let rowIndex = 0; rowIndex < model[selectedLayer].length; rowIndex++) {
        const currentRow = model[selectedLayer][rowIndex];
        let row = document.createElement("div");
        layerWrapper.appendChild(row);
        row.classList.add("row");
        row.dataset.rowid = rowIndex;
        for (let blockIndex = 0; blockIndex < currentRow.length; blockIndex++) {
            let block = document.createElement("div");
            block.dataset.blockid = blockIndex;
            if(model[selectedLayer][rowIndex][blockIndex]) {
                block.innerHTML = "<span title='block' class='selected' data-value='1'></span>";
            } else {
                block.innerHTML = "<span title='no block' data-value='0'></span>";
            }

            row.appendChild(block);
            block.addEventListener("click", toggleBlock)
        }

    }
}

function generateLayer() {
    const layer = []
    for (let index = 0; index < modelWidth; index++) {
        layer.push(Array(modelDepth).fill(0));
    }
    return layer;
}

function generateDefaultModel() {
    model = [generateLayer()]
    addAllLayersToDropdown();
    console.log("model: ", model);
}

function copyText(text) {
    document.addEventListener("copy", (e) => {
        e.clipboardData.setData("text/plain", (text));
        e.preventDefault();
        document.removeEventListener("copy", null);
        console.log("copied to clipboard")
    });
    document.execCommand("copy");
}

docReady(function() {
    document.getElementById("printModel").addEventListener("click", function() {
        copyText(JSON.stringify(model));
    });
    document.getElementById("layers").addEventListener("change", function(event) {
        console.log("layer changed")
        selectedLayer = event.target.value;
        displayLayer();
    });
    document.getElementById("addLayerButton").addEventListener("click", function() {
        console.log("Added a new layer")
        model.push(generateLayer());
        addAllLayersToDropdown();
    });

    generateDefaultModel()
    displayLayer();
});