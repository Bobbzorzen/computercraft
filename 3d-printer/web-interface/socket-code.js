const DELIMITER = "|||";
let selectedTurtle = undefined;

function buildMessage(messageType, content) {
    return message = messageType + DELIMITER + content.join(DELIMITER)
}

function addAllTurtlesToDropdown(turtles) {
    console.log("turtles recived: ", turtles);
    const turtlesDropdown = document.getElementById("selectableTurtles");

    turtlesDropdown.innerHTML = "";
    //Create and append the options
    for (var i = 0; i < turtles.length; i++) {
        var option = document.createElement("option");
        option.value = turtles[i];
        option.text = turtles[i];
        turtlesDropdown.appendChild(option);
    }
}

docReady(function() {
    let socket = new WebSocket("ws://94.254.47.77:8080");

    socket.onopen = function(e) {
        console.log("[open] Connection established");
        console.log("Sending to server");
        socket.send("interface" + DELIMITER + "web");
        socket.send("list");
    };

    socket.onmessage = function(event) {
        console.log("[message] Data received from server: ", event.data);
        const splitMessage = event.data.split(DELIMITER)
        if (splitMessage[0] === "list") {
            const listOfTurtles = JSON.parse(splitMessage[1]);
            addAllTurtlesToDropdown(listOfTurtles)
            if (!selectedTurtle) {
                selectedTurtle = listOfTurtles[0];
            }
        }
    };

    socket.onclose = function(event) {
        if (event.wasClean) {
            console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        } else {
            console.log('[close] Connection died');
        }
    };

    socket.onerror = function(error) {
        console.log(`[error] ${error.message}`);
    };
    document.getElementById("terminateTurtle").addEventListener("click", function() {
        socket.send(buildMessage("print", [selectedTurtle, "terminate"]));
    });
    document.getElementById("selectableTurtles").addEventListener("change", function(event) {
        console.log("select updated: ", event.target.options[event.target.selectedIndex].text);
        selectedTurtle = event.target.options[event.target.selectedIndex].text;
        requestInventoryUpdate(socket);
    });
    document.getElementById("printModel").addEventListener("click", function() {
        socket.send(buildMessage("print", [selectedTurtle, JSON.stringify(model)]));
    });
});