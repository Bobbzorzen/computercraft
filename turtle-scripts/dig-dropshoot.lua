local dig = require "dig-utilities"

local tArgs = {...}

-- Stores argument as numbers
local depth = tonumber(tArgs[1])

local function digShoot()
    for _ = 1, (depth - 1) do
        dig.down()
        turtle.down()
    end
    -- Place bucket?
    turtle.select(16)
    turtle.placeDown();
end

local function returnToSurface()
    for _ = 1, (depth - 1) do
        turtle.up()
    end
end

digShoot()
returnToSurface()