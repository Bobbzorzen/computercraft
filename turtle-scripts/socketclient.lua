environment = require "environment"

ws, err = http.websocket(environment.connectionString)
delimiter = "|||"

function loadCode(code, environ)
    local f = assert(loadstring(code))
    setfenv(f, environ)
    safeExecuteFunction(f)
end
function safeExecuteFunction(funcArg)
    local ran, errorMsg = pcall( funcArg )
    if not ran then
        print("Unable to execute code\n\n" .. errorMsg)
    end
end

if ws then
    -- Probably should do something interesting with the websocket before
    print("Socket established, sending identification")
    ws.send("turtle" .. delimiter .. os.getComputerLabel())
    while true do
        local _, url, response, isBinary = os.pullEvent("websocket_message")
        if (response == "terminate") then
            print("response: " .. response)
            break
        else
            print("response: " .. response)
            context = {
                ws = ws,
                delimiter = delimiter,
                turtle = turtle,
                textutils = textutils,
                print = print
            }
            loadCode(response, context) -- Run code with context variables
        end
    end
    print("Closing socket")
    ws.close()
else
    print("no socket available")
    print(err)
end
