tArgs = {...}

local url = tArgs[1]
local fileName = tArgs[2]

local request = http.get(url)
local fetchedProgram = request.readAll()
request.close()

local file = fs.open(fileName, "w")
file.write(fetchedProgram)
file.close()