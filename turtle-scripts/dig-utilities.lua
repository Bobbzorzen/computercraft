utilities = require "utilities"

local dig = {}

function dig.up()
    while(turtle.detectUp()) do
        turtle.digUp()
    end
end

function dig.down()
    while(turtle.detectDown()) do
        turtle.digDown()
    end
end

function dig.forward()
    while(turtle.detect()) do
        turtle.dig()
    end
end

function dig.left()
    turtle.turnLeft()
    dig.forward()
    turtle.turnRight()
end

function dig.right()
    turtle.turnRight()
    dig.forward()
    turtle.turnLeft()
end


function dig.row(distance, refuel, enderChestSlot)
    enderChestSlot = enderChestSlot or 0
    refuel = refuel or 0
    for i = 1, (distance - 1) do
        dig.forward()
        turtle.forward()
    end
    for i = 1, (distance - 1) do
        turtle.back()
    end
    if(refuel ~= 0) then
        utilities.refuelAll()
    end
    if(enderChestSlot ~= 0) then
        utilities.emptyToEnderchest(enderChestSlot)
    end
end

function dig.layer(width, depth, refuel, enderChestSlot)
    for i = 1, (width - 1) do
        dig.row(depth, refuel, enderChestSlot)
        turtle.turnRight()
        dig.forward()
        turtle.forward()
        turtle.turnLeft()
    end
    dig.row(depth, refuel, enderChestSlot)
    turtle.turnLeft()
    for i = 1, (width - 1) do
        turtle.forward()
    end
    turtle.turnRight()
end

return dig