local blocksMined = 0;

function digLayer()
    for i = 1, 4 do
        turtle.dig()
        turtle.forward()
        turtle.turnRight()
    end
end

local function moveAndDig()
    turtle.digUp()
    turtle.up()
    blocksMined = blocksMined + 1
    digLayer()
end

turtle.dig()
turtle.forward()
digLayer()
while (turtle.compareUp(1)) do
    moveAndDig()
end
moveAndDig()

for _ = 1, blocksMined do
    turtle.down()
end
turtle.back()
