utilities = require "utilities"
dig = require "dig-utilities"

tArgs = {...}

-- Stores argument as numbers
local depth = tonumber(tArgs[1])
local width = tonumber(tArgs[2])
local height = tonumber(tArgs[3])

function digRoom()
    for i = 1, (height - 1) do
        dig.layer(width, depth, 1, 16)
        dig.up()
        turtle.up()
    end
    dig.layer(width, depth, 1, 16)
    for i = 1, (height - 1) do
        turtle.down()
    end
end

digRoom()
