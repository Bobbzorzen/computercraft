utilities = require "utilities"
dig = require "dig-utilities"

tArgs = {...}

-- Stores argument as numbers
local depth = tonumber(tArgs[1])
local width = tonumber(tArgs[2])
local height = tonumber(tArgs[3])

function goUpAndDropOff(heightToSurface)
    for i = 1, (heightToSurface - 1) do
        turtle.up()
    end
end

function quarry()
    for i = 1, (height - 1) do
        dig.layer(width, depth, 1, 0)
        dig.down()
        turtle.down()
        if not utilities.hasEmptySlots() then
            goUpAndDropOff(i)
        end
    end
    dig.layer(width, depth, 1, 0)
    for i = 1, (height - 1) do
        turtle.up()
    end
end

quarry()
