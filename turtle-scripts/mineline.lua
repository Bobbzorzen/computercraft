tArgs = {...}

-- Stores arguments as numbers
local distance = tonumber(tArgs[1])

for i = 1, (distance - 1) do
    turtle.dig()
    turtle.forward()
    turtle.digUp()
end
for i = 1, (distance - 1) do
    turtle.back()
end
