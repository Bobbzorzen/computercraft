function digQuartzForward()
    if (turtle.compare() ~= true) then
        print("Found quartz!")
        turtle.dig()
    end
end

function digQuartzDown()
    if (turtle.compareDown() ~= true) then
        print("Found quartz!")
        turtle.digDown()
    end
end

function rotateAroundCorner()
    print("Rotating around corner")
    turtle.turnRight()
    turtle.forward()
    turtle.turnLeft()
    turtle.forward()
    turtle.turnLeft()
end

function digBottomQuartz()
    print("Digging bottom Quartz")
    turtle.back()
    turtle.down()
    turtle.down()
    turtle.forward()
    digQuartzForward()
    turtle.back()
    turtle.up()
    turtle.up()
    turtle.forward()

end

print("Starting quartz automation")
while true do
    turtle.select(1)
    print("Starting compare")
    digQuartzForward()
    digQuartzDown()
    rotateAroundCorner()
    digQuartzDown()
    rotateAroundCorner()
    digQuartzDown()
    rotateAroundCorner()
    digQuartzDown()
    rotateAroundCorner()
    digQuartzDown()
    digBottomQuartz()
    print("Sleeping for 60 seconds")
    sleep(60)
end
