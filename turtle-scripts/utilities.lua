local utilities = {}

function utilities.refuelAll()
    for i = 1, 16 do
        turtle.select(i)
        turtle.refuel()
    end
    turtle.select(1)
end

function utilities.emptyToEnderchest(slot)
    turtle.dig()
    turtle.select(slot)
    turtle.place()
    for i = 1, 14 do
        turtle.select(i)
        turtle.drop()
    end
    turtle.select(slot)
    turtle.dig()
    turtle.select(1)
end

function utilities.emptyToChest()
    for slot = 1, 16 do
        turtle.select(slot)
        turtle.drop()
    end
end

function utilities.hasEmptySlots()
    for slot = 1, 16 do
        if turtle.getItemCount(slot) == 0 then
            return true
        end
    end
    return false
end

return utilities
