local blocksMined = 0;

local function digUp()
    while (turtle.compareUp(1)) do
        turtle.digUp()
        turtle.up()
        blocksMined = blocksMined + 1
    end
    turtle.digUp()
    turtle.up()
    blocksMined = blocksMined + 1
end

local function digDown()
    for _ = 1, blocksMined do
        turtle.digDown()
        turtle.down()
    end
    blocksMined = 0
end

local function digUpDown()
    turtle.dig()
    turtle.forward()
    digUp()
    turtle.dig()
    turtle.forward()
    digDown()
end

local function reposition()
    turtle.dig()
    turtle.forward()
    turtle.turnRight()
    turtle.dig()
    turtle.forward()
    turtle.turnRight()
end
digUpDown()
reposition()
digUpDown()
reposition()
