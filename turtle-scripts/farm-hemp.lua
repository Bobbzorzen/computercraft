utilities = require "utilities"
dig = require "dig-utilities"

tArgs = {...}

-- Stores argument as numbers
local farmSize = tonumber(tArgs[1])

function farmRow()
    for i = 1, farmSize - 1 do
        dig.down()
        turtle.forward()
    end
    dig.down()
    for i = 1, farmSize - 1 do
        turtle.back()
    end
end

function farmLayer()
    for i = 1, farmSize-1 do
        farmRow()
        turtle.turnRight()
        turtle.forward()
        turtle.turnLeft()
    end
    farmRow()
    turtle.turnLeft()
    for i = 1, farmSize -1 do
        turtle.forward()
    end
    turtle.turnRight()
end

function farmHemp()
    while true do
        farmLayer()
        utilities.emptyToEnderchest(16)
        for i = 10, 1, -1 do
            print("Sleeping for " .. i .. " minutes")
            sleep(60)
        end
    end
end
farmHemp()
