local dig = require "dig-utilities"

local tArgs = {...}

-- Stores argument as numbers
local depth = tonumber(tArgs[1])

local function digStairLayer() 
    for _ = 1, 4 do
        dig.left()
        dig.forward()
        dig.right()
        turtle.forward()
    end
    dig.left()
    dig.right()
    for _ = 1, 3 do
        turtle.back()
    end
end

local function digStair()
    for _ = 1, (depth - 1) do
        digStairLayer()
        dig.down()
        turtle.down()
    end
    digStairLayer()
    turtle.back()
end

digStair()