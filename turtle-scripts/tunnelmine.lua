local utilities = require "utilities"
local dig = require "dig-utilities"
-- Fetches argument list
local tArgs = {...}

-- Stores arguments as numbers
local distance = tonumber(tArgs[1])
local enderChestSlot = tonumber(tArgs[2])
enderChestSlot = enderChestSlot or 0

print("starting script, mining " .. distance .. " blocks")

local function addTorches()
    turtle.select(16)
    turtle.up()
    turtle.turnLeft()
    turtle.place()
    turtle.turnRight()
    turtle.turnRight()
    turtle.place()
    turtle.turnLeft()
    turtle.select(1)
    turtle.down()
end

local function digLeftRight()
    turtle.turnLeft()
    dig.forward()
    turtle.turnRight()
    turtle.turnRight()
    dig.forward()
    turtle.turnLeft()
end

local function digRow()
    print("digging row")
    dig.up()
    digLeftRight()
    turtle.up()
    dig.up()
    digLeftRight()
    turtle.up()
    digLeftRight()
    turtle.down()
    turtle.down()
end

for i = 1, distance do
    turtle.dig();
    dig.forward()
    turtle.forward()
    digRow()
    if(enderChestSlot ~= 0) then
        utilities.emptyToEnderchest(enderChestSlot)
    end
    if(i % 8 == 0) then
        addTorches()
    end
end
for i = 1, distance do
    turtle.back()
end
